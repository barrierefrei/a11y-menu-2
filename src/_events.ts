/**
 * Custom events
 * @example
 *   // listen to local events bound to the menu instance
 *   const myMenu = new A11yMenu2({ ... })
 *   myMenu.onEvent('onSubmenuOpen', (options) => {
 *     console.log('A submenu was opened', options);
 *     console.log('The trigger is:', options.currentTrigger);
 *     console.log('The submenu is:', options.currentSubmenu);
 *     console.log('The navigation is:', options.nav);
 *   })
 *
 *   // or listen to global events on the document
 *   document.addEventListener('a11yMenuSubmenuOpen', (options) => {
 *     console.log('A submenu was opened', options);
 *     console.log('The trigger is:', options.currentTrigger);
 *     console.log('The submenu is:', options.currentSubmenu);
 *     console.log('The navigation is:', options.nav);
 *   })
 */
export const events = {
  /** Any submenu was opened and now something is open. */
  onSubmenuOpen: 'a11yMenuSubmenuOpen',

  /** Any submenu was closed. Something might still be open. */
  onSubmenuClose: 'a11yMenuSubmenuClose',

  /** Something was closed and now everything is closed. */
  onCloseAll: 'a11yMenuCloseAll',

  /** The menu is ready to be served */
  onReady: 'a11yMenuReady',
};
