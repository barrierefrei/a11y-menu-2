import { events } from './_events';
import { onClickOutside, stripHash, stripTrailingSlash } from './_helper';

/** Custructur parameters */
interface ParameterOptions {
  /**
   * The current `<nav>` element
   * @example
   *    ```html
   *    <nav aria-label="Main navigation"></nav>
   *    ```
   */
  nav: HTMLElement;

  /**
   * Include optional support for arrow keys, Home, and End. Default is `true`.
   * Note: currently only Home and End are supported.
   */
  keyboardSupport?: boolean;

  /**
   * Each submenu is hidden by default when another submenu of the same level is opened to avoid collisions. You may overwrite this behaviour, by using a function that returns a boolean. The function will be called every time, a trigger for a submenu is triggered.
   * @example
   *   collapseAllCondition: () => window.innerWidth > 1280 // collapse only when >1280px
   */
  collapseAllCondition?: () => boolean;
}

/** Event callback options */
interface EventCallbackOptions {
  nav: ParameterOptions['nav'];

  /**
   * The current trigger element
   * @example
   *   ```html
   *   <button aria-expanded="false" aria-controls="foo"></button>
   *   <button aria-expanded="true" aria-controls="foo"></button>
   *   ```
   */
  currentTrigger?: HTMLButtonElement;

  /**
   * The current submenu element
   * @example
   *   ```html
   *   <ul id="foo" inert></ul>
   *   <ul id="foo"></ul>
   *   ```
   */
  currentSubmenu?: HTMLUListElement;
}

/**
 * Accessible menu. The menu is controlled with the tab keys. Changed states are mapped via ARIA. Based on {@link https://www.w3.org/WAI/ARIA/apg/patterns/disclosure/examples/disclosure-navigation/}
 * @see {@link https://www.npmjs.com/package/@barrierefrei/a11y-menu-2}
 * @example
 *   const myMenu = new A11yMenu2(options)
 */
export class A11yMenu2 {
  private nav: ParameterOptions['nav'];
  private keyboardSupport: ParameterOptions['keyboardSupport'];
  private collapseAllCondition: ParameterOptions['collapseAllCondition'];
  private currentTrigger?: EventCallbackOptions['currentTrigger'];
  private currentSubmenu?: EventCallbackOptions['currentSubmenu'];

  /** The {@link nav} is active as long as it contains the `document.activeElement` or any submenu is still open. */
  private isActive = false;

  /** Indicates whether we are listening for a click outside of the {@link nav} */
  private listeningForClickOutside = false;

  /** Focus timeout for `focus` in Chrome. Time in milliseconds. */
  private focusTimeout = 1;

  /** Callback for click outside */
  private removeOnClickOutside?: () => void;

  constructor(params: ParameterOptions) {
    /** Check params */
    if (params.nav.nodeName !== 'NAV')
      throw 'Parameter nav requires a <nav> element';

    /** Assign params */
    this.nav = params.nav;
    this.collapseAllCondition = params.collapseAllCondition;
    this.keyboardSupport = params.keyboardSupport === false ? false : true; // default is `true`

    /** Init */
    this.init().then(() => {
      /** Fire ready event */
      this.dispatchEvent('onReady');
    });
  }

  /**
   * Get a submenu for a {@link trigger}
   * @param trigger Any trigger
   * @returns The submenu associated with the {@link trigger}
   * @example
   *   this.getSubmenu(trigger)
   */
  private getSubmenu(trigger: HTMLButtonElement) {
    return <HTMLUListElement>(
      this.nav.querySelector(
        `[id="${<string>trigger.getAttribute('aria-controls')}"]`,
      )
    );
  }

  /**
   * Close a submenu associated with any {@link trigger}
   * @param trigger Any trigger
   * @param focus Focus on the trigger after the submenu was closed
   * @example
   *   this.closeSubmenu(trigger)
   *   this.closeSubmenu(trigger, true) // with focus event
   */
  private closeSubmenu(trigger: HTMLButtonElement, focus?: boolean) {
    /** Exit when already collapsed */
    if (trigger.getAttribute('aria-expanded') === 'false') return;

    /** Collapse trigger */
    trigger.setAttribute('aria-expanded', 'false');

    /** Hide submenu */
    this.getSubmenu(trigger).inert = true;

    if (focus) {
      this.focus(trigger).then(() => {
        /** Fire event */
        this.dispatchEvent('onSubmenuClose');

        /** Run update */
        this.afterUpdate();
      });
    } else {
      /** Fire event */
      this.dispatchEvent('onSubmenuClose');

      /** Run update */
      this.afterUpdate();
    }
  }

  /**
   * Open a submenu associated with any {@link trigger}
   * @param trigger Any trigger
   * @example
   *   this.openSubmenu(trigger)
   */
  private openSubmenu(trigger: HTMLButtonElement) {
    /** Exit when already expanded */
    if (trigger.getAttribute('aria-expanded') === 'true') return;

    /** Set {@link nav} active state */
    this.isActive = true;

    /** If the {@link trigger} is a top level trigger, we should close all other top level submenus */
    if (
      (this.collapseAllCondition === undefined ||
        this.collapseAllCondition.call(this) === true) &&
      this.getTriggers(true).includes(trigger)
    )
      this.closeAll();

    /** Expand trigger */
    trigger.setAttribute('aria-expanded', 'true');

    const submenu = this.getSubmenu(trigger);

    /** Show submenu */
    submenu.inert = false;

    /** Store the current submenu so we can do stuff with it later */
    this.currentSubmenu = submenu;

    /** Store the current trigger so we can do stuff with it later */
    this.currentTrigger = trigger;

    /** Fire event */
    this.dispatchEvent('onSubmenuOpen');

    /** Start to listen for any click outside of the navigation */
    if (!this.listeningForClickOutside) {
      this.removeOnClickOutside = onClickOutside(this.nav, () => {
        this.closeAll();
      });

      this.listeningForClickOutside = true;
    }

    /** Run update */
    this.afterUpdate();
  }

  /**
   * Do stuff with buttons that trigger submenus
   * @example
   *   this.handleTriggers()
   */
  private handleTriggers() {
    this.getTriggers().forEach((trigger) => {
      const onClick = () => {
        if (trigger.getAttribute('aria-expanded') === 'false') {
          this.openSubmenu(trigger);
        } else {
          this.closeSubmenu(trigger, true);
        }
      };

      /** Toggle aria-expanded on click */
      trigger.addEventListener('click', onClick.bind(this));
    });
  }

  private onKeydown(e: KeyboardEvent) {
    /** Prevent keyboard repeat */
    if (!e.repeat) {
      if (e.code === 'Escape') {
        this.closeAll(true);
      }

      /** Only allow inside the current navigation */
      if (document.activeElement && this.nav.contains(document.activeElement)) {
        /** Include optional support for arrow keys, Home, and End */
        if (this.keyboardSupport) {
          switch (e.code) {
            case 'Home':
              e.preventDefault();

              /** If focus is on a toplevel link/button, and it is not the first link/button, moves focus to the first link/button. */
              const firstElement = <HTMLElement>(
                this.nav
                  .querySelector(':scope > ul > li')
                  ?.querySelector('a, button')
              );
              this.focus(firstElement);
              break;

            case 'End':
              e.preventDefault();

              /** If focus is on a toplevel link/button, and it is not the last link/button, moves focus to the last link/button. */
              const lastElement = <HTMLElement>(
                this.nav
                  .querySelector(':scope > ul > li:last-child')
                  ?.querySelector('a, button')
              );
              this.focus(lastElement);
              break;

            case 'ArrowUp':
            case 'ArrowLeft':
              /** TODO: add Tab + Shift behaviour. We cannot re-map the Tab key for security reasons. */
              break;

            case 'ArrowDown':
            case 'ArrowRight':
              /** TODO: add Tab behaviour. We cannot re-map the Tab key for security reasons. */
              break;

            default:
              break;
          }
        }
      }
    }
  }

  /**
   * Do stuff after some state changed
   * @example
   *   this.afterUpdate()
   */
  private afterUpdate() {
    if (!this.isActive) return;

    const hasOpenSubmenu = this.hasOpenSubmenu();

    this.isActive =
      (document.activeElement && this.nav.contains(document.activeElement)) ||
      hasOpenSubmenu
        ? true
        : false;

    if (!hasOpenSubmenu) {
      this.dispatchEvent('onCloseAll');
    }

    if (!this.isActive) {
      // remove event listener for outside click
      if (this.removeOnClickOutside) this.removeOnClickOutside();
      this.listeningForClickOutside = false;
    }
  }

  /**
   * Checks if the navigation has any open submenus
   * @param topLevel Return only the top level triggers
   * @returns Returns `true` if any were found
   * @example
   *   this.hasOpenSubmenu()
   */
  private hasOpenSubmenu(topLevel?: boolean) {
    return this.getTriggers(topLevel).find((x) =>
      this.nav.querySelector(
        `[id="${<string>x.getAttribute('aria-controls')}"]:not([inert])`,
      ),
    )
      ? true
      : false;
  }

  /**
   * Compare any {@link el} with any URL. Return `true` if the {@link el}s `href` matches that URL. Adds attribute `[data-current-parent="true"]` to all ancestors. Adds attribute `[aria-current="page"]` to the {@link el}.
   * @param url Any URL
   * @param el Any anchorlink
   * @param force By default this function only applies current states to links that actually match the {@link url}. When set to `true` will apply anyways and ignore {@link url}.
   * @example
   *   ```html
   *   <a href="/my-page"></a>
   *   ```
   *   handleCurrentPage(window.location.href, menuItem)
   *   ```html
   *   <a aria-current="page" href="/my-page"></a>
   *   ```
   */
  private handleCurrentPage = (
    url: string,
    el: HTMLAnchorElement,
    force?: boolean,
  ) => {
    const page = new URL(url);
    if (!force && !page) return;
    if (el.nodeName !== 'A') return;
    if (!el.hasAttribute('href')) return;

    /** `true` if {@link el} is the current page {@link MenuItem} */
    let isCurrent = false;

    const href = el.getAttribute('href');

    if (!force && href) {
      /** Exclude anchor links */
      if (href.startsWith('#')) return;

      if (href.startsWith('/')) {
        if (href.startsWith('//')) {
          if (!page.protocol) return;

          /** Handle URLs with host and without http(s) */
          const menuItemUrl = new URL(page.protocol + href);

          if (menuItemUrl && menuItemUrl.host === page.host) {
            /** URL host matches so it is an internal URL */
            if (
              stripTrailingSlash(stripHash(menuItemUrl.pathname)) ===
              stripTrailingSlash(stripHash(page.pathname))
            ) {
              isCurrent = true;
            }
          }
        } else {
          /** Handle absolute urls */
          if (
            stripTrailingSlash(stripHash(href)) ===
            stripTrailingSlash(stripHash(page.pathname))
          ) {
            isCurrent = true;
          }
        }
      } else {
        if (href.startsWith('http')) {
          /** Handle URLs with host and with http(s) */
          const menuItemUrl = new URL(href);

          if (menuItemUrl && menuItemUrl.host === page.host) {
            /** URL host matches so it is an internal URL */
            if (
              stripTrailingSlash(stripHash(menuItemUrl.pathname)) ===
              stripTrailingSlash(stripHash(page.pathname))
            ) {
              isCurrent = true;
            }
          }
        } else {
          /** We do not need to handle relative urls */
        }
      }
    }

    if (isCurrent === true || force) {
      /** We found a {@link MenuItem} that is very likely the current page. We can now mark all parent items so we can apply styles to them */
      let item: Element = el;

      item.setAttribute('aria-current', 'page');

      while (item.parentElement && item.parentElement !== this.nav) {
        /** Each parent {@link MenuItem} will have a `data-current-parent="true"` attribute */
        if (item.parentElement.nodeName === 'LI') {
          item.parentElement.dataset.currentParent = 'true';
        }
        item = item.parentElement;
      }
    }
  };

  private get links() {
    return Array.from(this.nav.querySelectorAll('a'));
  }

  /**
   * Set attributes on various element inside the {@link nav}
   * @example
   *   this.attributes()
   */
  private setAttributes() {
    /**
     * Set `[aria-current="page"]` and `[data-current-parent="true"]`
     * @see {@link https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-current}
     */
    this.links.forEach((x) => {
      this.handleCurrentPage(window.location.href, <HTMLAnchorElement>x);
    });
  }

  /**
   * Add event listeners
   * @example
   *   this.addEventListeners()
   */
  private addEventListeners() {
    /** Keys */
    document.addEventListener('keydown', this.onKeydown.bind(this));

    /** Clicks */
    this.links.forEach((link) => {
      const onLink = () => {
        // exclude external links (because they could never be the current page)
        if (link.hasAttribute('href')) {
          const linkUrl = new URL(link.href);
          const currentUrl = new URL(window.location.href);
          console.log(currentUrl.origin, linkUrl.origin);
          if (currentUrl.origin !== linkUrl.origin) return;
        }

        /** If any link has `[aria-current]` set, remove it. */
        this.links
          .filter((fi) => fi.hasAttribute('aria-current'))
          .forEach((xi) => {
            xi.removeAttribute('aria-current');
          });

        /** If any element has `[data-current-parent]` set, remove it. */
        Array.from(this.nav.querySelectorAll('[data-current-parent]')).forEach(
          (dp) => {
            dp.removeAttribute('data-current-parent');
          },
        );

        /** Sets `[aria-current="page"]` on the focused link. */
        this.handleCurrentPage(link.href, link, true);
      };

      link.addEventListener('click', onLink);
    });
  }

  private timeout(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  /**
   * Init
   * @example
   *   this.init()
   */
  private async init() {
    this.setAttributes();
    this.addEventListeners();
    this.handleTriggers();

    /** Wait a bit so JavaScript can run */
    await this.timeout(100);
  }

  /**
   * Focus on any element. Make it focussable, if it isn't, then focus
   * @param el Any element
   * @example
   *   const el = document.getElementById('foo')
   *   this.focus(el).then(() => {
   *     console.log('focus is now on element', el)
   *   })
   */
  private async focus(el?: HTMLElement) {
    if (!el) return;

    /** Make element focussable */
    if (el.tabIndex === undefined) el.tabIndex = 0;

    setTimeout(() => {
      /** Focus after timeout to support Chrome */
      el.focus();
    }, this.focusTimeout);
  }

  /**
   * Get all triggers for submenus in the naviagtion
   * @param topLevel Return only the top level triggers
   * @returns Triggers
   * @example
   *   this.getTriggers()
   *   this.getTriggers(true) // Top level only
   */
  private getTriggers(topLevel?: boolean) {
    return Array.from(
      this.nav.querySelectorAll(
        topLevel
          ? ':scope > ul > li > button[aria-expanded][aria-controls]'
          : 'button[aria-expanded][aria-controls]',
      ),
    ).map((x) => <HTMLButtonElement>x);
  }

  /**
   * Dispatch a custom event. Checkout {@link events} for documentation on how to listen to these custom events.
   * @param event Event name from {@link events}
   * @example
   *   this.dispatchEvent('onSubmenuOpen')
   */
  private dispatchEvent(event: keyof typeof events) {
    const customEvent = new Event(events[event]);

    /** Fire custom `Event` on {@link nav} */
    this.nav.dispatchEvent(customEvent);

    /** Fire custom `Event` on {@link document} */
    document.dispatchEvent(customEvent);
  }

  /**
   * Event handling
   * @param event A11Y Menu 2 Event from {@link events}
   * @param callback Any callback function
   * @example
   *   const myMenu = new A11yMenu2(options)
   *   myMenu.onEvent('onCloseAll', (options) => { console.log('All submenus are closed', options) })
   */
  onEvent(
    event: keyof typeof events,
    callback: (options: EventCallbackOptions) => void,
  ) {
    /** When custom `Event` triggers */
    this.nav.addEventListener(events[event], () => {
      callback.call(this, {
        currentTrigger: this.currentTrigger,
        currentSubmenu: this.currentSubmenu,
        nav: this.nav,
      });
    });
  }

  /**
   * Collapse all submenu items
   * @param focus Focus on the last submenu trigger that was expanded
   * @example
   *   const myMenu = new A11yMenu2()
   *   document.querySelector("button").addEventListener('click', () => {
   *     myMenu.closeAll()
   *   })
   */
  closeAll(focus?: boolean) {
    this.getTriggers().forEach((trigger) => {
      this.closeSubmenu(trigger, focus);
    });
  }
}
