/**
 * Detect click outside of an element
 * @param el Any element
 * @param callback Callback function
 * @returns Function that removes all events listeners that were added by this function
 * @example
 *   const removeOnClickOutside = onClickOutside(document.getElementById('foo'), () => {
 *     console.log('you clicked outside of the element')
 *   }) // now listening
 *   removeOnClickOutside() // not listening anymore
 */
export const onClickOutside = (el: HTMLElement, callback?: () => void) => {
  /**
   * Clicking on the {@link el} or any child will do nothing
   * @param e Click event
   * @returns –
   * @example
   *   el.removeEventListener('mouseup', clickInside);
   */
  const clickInside = (e: MouseEvent) => e.stopPropagation();

  /**
   * Clicking outside of the {@link el} will call the callback function and remove the event listeners attached
   * @example
   *   document.addEventListener('mouseup', clickOutside, { once: true });
   */
  const clickOutside = () => {
    if (callback) callback.call(this);
    el.removeEventListener('mouseup', clickInside);
  };

  document.addEventListener('mouseup', clickOutside, { once: true });
  el.addEventListener('mouseup', clickInside);

  /**
   * Remove all event listeners
   * @returns –
   * @example
   *   cleanup()
   */
  const cleanup = () => el.removeEventListener('mouseup', clickInside);

  return cleanup;
};

/**
 * Strip a hash (`'#foo'`) from a string
 * @param str Any string
 * @returns First part of {@link str} without # or any following characters
 * @example
 *   stripHash('example.com/en/#foo') // 'example.com/en/'
 */
export const stripHash = (str: string) =>
  str.includes('#') ? str.split('#')[0] : str;

/**
 * Strip the trailing slash (`'/'`) from a string
 * @param str Any string
 * @returns String without trailing `'/'`
 * @example
 *   stripTrailingSlash('example.com/en/') // 'example.com/en'
 */
export const stripTrailingSlash = (str: string) =>
  str.endsWith('/') ? str.slice(0, -1) : str;
