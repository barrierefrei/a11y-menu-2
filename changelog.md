# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v1.3.0] - 2025-01-02

### Fixed

- Clicking on an external link will no longer think of that link as the current page (because an external link can never be the current page)

### Changed

- Improved the documentation
- Custom events will now be fired on the `<nav>` element **and** also the `document`. So you can listen to events on local and global scopes.

## [v1.2.0] - 2023-10-06

### Added

- A new event `onReady` that fires after all the JavaScript from the `init` function is run.

### Fixed

- When pressing *Home* or *End* key, we do now prevent the default browser behaviour

## [v1.1.0] - 2023-09-15

### Changed

- Simplified markup to be even more flexible, following the instructions on the [„WCAG: Disclosure Navigation Menu”](https://www.w3.org/WAI/ARIA/apg/patterns/disclosure/examples/disclosure-navigation/#mythical-page-content)

## [v1.0.1] - 2023-09-14

### Added

- Submenus of the same level will collapse by default. Created a new param `collapseAllCondition` that can be used to alter this behaviour. We can for example decide to collapse menu items only on `window.innerWidth >= 1280`

### Changed

- Allow submenus without trigger elements, when they are visible by default
- Updated README

## [v1.0.0] - 2023-09-14

- Initial release

[unreleased]: https://gitlab.com/barrierefrei/a11y-menu-2
[v1.3.0]: https://gitlab.com/barrierefrei/a11y-menu-2/-/compare/v1.3.0...v1.2.0
[v1.2.0]: https://gitlab.com/barrierefrei/a11y-menu-2/-/compare/v1.2.0...v1.1.0
[v1.1.0]: https://gitlab.com/barrierefrei/a11y-menu-2/-/compare/v1.1.0...v1.0.1
[v1.0.1]: https://gitlab.com/barrierefrei/a11y-menu-2/-/compare/v1.0.1...v1.0.0
[v1.0.0]: https://gitlab.com/barrierefrei/a11y-menu-2/-/tree/v1.0.0
